# Metabob Analysis Pipe

This pipe automatically performs Metabob Analysis on your codebase

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: metabob/metabob-bitbucket-pipe:0.4.0
  variables:
    API_KEY: "<string>"
    DEBUG: "<boolean>" # Optional
    FAIL_ON_TIMEOUT: "<boolean>" # Optional
    FAIL_THRESHOLD: "<integer>" # Will report as failed if the number of bugs is greater than or equal to this number, default 0
    API_URL: "<string>" # Optional
    POLLING_TIMEOUT: "<integer>" # Time in seconds before aborting the analysis, default 600
    POLLING_INTERVAL_SECONDS: "<integer>" # TIme in seconds between polling the AI server, default 5
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| API_KEY (*)           | The name that will be printed in the logs |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
| FAIL_ON_TIMEOUT       | Fail if Metabob does not complete the anaysis within the timeout. Default: `false`. |
| FAIL_THRESHOLD        | Will cause the report to FAIL if the number of identified problems exceeds this amount. Default: 0 |
| POLLING_TIMEOUT       | Time in seconds before aborting the analysis. Default: 600 |
| POLLING_INTERVAL_SECONDS      | TIme in seconds between polling the AI server, default 5 |
| CREATE_REPORT | Whether to post a report to Bitbucket CodeInsights |
| GENERATE_RECOMMENDATION | Whether to generate a recommendation. Default: 'false'. |

_(*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: metabob/metabob-bitbucket-pipe:0.4.0
    variables:
      API_KEY: $METABOB_API_KEY
```

Advanced example:

```yaml
script:
  - pipe: metabob/metabob-bitbucket-pipe:0.4.0
    variables:
      API_KEY: $METABOB_API_KEY"
      FAIL_THRESHOLD: 4
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by avi@metabob.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
