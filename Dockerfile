FROM python:3.9-slim as base

RUN apt-get update && apt-get install -y git && rm -rf /var/lib/apt/lists/*

FROM base

WORKDIR /opt/app/

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY pipe .

COPY LICENSE.txt . 

COPY pipe.yml .

COPY README.md .

ENTRYPOINT ["python", "pipe.py"]
