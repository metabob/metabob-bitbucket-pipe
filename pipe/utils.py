"""
{
    "path": "string",
    "category": "string",
    "startLine": 0,
    "endLine": 0,
    "id": "string",
    "summary": "string",
    "description": "string",
    "discarded": false,
    "endorsed": false
}
"""


def convert_metabob_problems_to_code_annotations(mb_problems: list[dict]) -> list[dict]:
    ret = []
    for problem in mb_problems:
        annotation = {
            "type": "report_annotation",
            "annotation_type": "BUG",
            "external_id": problem["id"],
            "summary": problem["summary"],
            "path": problem["path"],
            "line": problem["startLine"],
        }
        ret.append(annotation)
    return ret
