from bitbucket_pipes_toolkit import Pipe, get_logger, CodeInsights
import requests
from zipfile import ZipFile, ZIP_DEFLATED
from pathlib import Path
from utils import convert_metabob_problems_to_code_annotations
import time
import json
from uuid import uuid4
from urllib.parse import urljoin
import subprocess

logger = get_logger()

schema = {
    "API_KEY": {"type": "string", "required": True},
    "DEBUG": {"type": "boolean", "required": False, "default": False},
    "FAIL_ON_TIMEOUT": {"type": "boolean", "required": False, "default": False},
    "FAIL_THRESHOLD": {"type": "integer", "required": False, "default": 0},
    "API_URL": {
        "type": "string",
        "required": False,
        "default": "https://ide.metabob.com/",
    },
    "CREATE_REPORT": {"type": "boolean", "required": False, "default": False},
    "OUTPUT_FILE": {
        "type": "string",
        "required": False,
        "default": "reports/metabob-results.json",
    },
    "POLLING_TIMEOUT": {"type": "integer", "required": False, "default": 180},
    "POLLING_INTERVAL_SECONDS": {"type": "integer", "required": False, "default": 5},
}


class MetabobPipe(Pipe):
    def run(self):
        super().run()
        logger.setLevel(10 if self.get_variable("DEBUG") else 20)
        logger.info("Starting up...")
        logger.info("Preparing Codebase")
        self.prepare_archive()
        logger.info("Creating Analysis Request")
        problems = self.run_analysis()
        if self.get_variable("CREATE_REPORT"):
            logger.info("Creating Annotations")
            try:
                self.create_report(problems)
            except requests.exceptions.HTTPError:
                logger.critical("Failed to create report!")
        self.print_results(problems)
        self.success(message="Success!")

    def get_identifiers(self) -> tuple[str]:
        workspace = self.env["BITBUCKET_WORKSPACE"]
        repo = self.env["BITBUCKET_REPO_SLUG"]
        sha = self.env["BITBUCKET_COMMIT"]
        return (workspace, repo, sha)

    def get_full_name(self) -> str:
        workspace, repo, sha = self.get_identifiers()
        return f"{workspace}:{repo}:{sha}"

    def setup_analysis(self) -> str:
        api_url = self.get_variable("API_URL")
        api_key = self.get_variable("API_KEY")
        session_url = urljoin(api_url, "session")
        resp = requests.post(session_url, json={"apiKey": api_key})
        if resp.status_code == 200:
            return resp.json()["session"]
        else:
            return None

    def run_analysis(self) -> list[dict]:
        api_url = self.get_variable("API_URL")

        session_info = None
        for _ in range(5):
            session_info = self.setup_analysis()
            if session_info is not None:
                break
            time.sleep(5)

        headers = {
            "Authorization": f"Bearer {session_info}",
        }
        analysis_url = urljoin(api_url, "submit")
        repo_dir = Path(self.env["BITBUCKET_CLONE_DIR"]).resolve(strict=True)

        def get_results_for(
            analysis_url: str, headers: dict, repo_dir: Path, file: str
        ) -> list[dict]:
            resp = requests.post(
                analysis_url,
                headers=headers,
                files={'upload': (repo_dir / file).open("rb")},
            )
            if resp.status_code == 200:
                data = resp.json()
                return data.get("results", [])
            return []

        results = []
        for file in self.get_changed_files():
            results.extend(get_results_for(analysis_url, headers, repo_dir, file))

        return results

    def get_changed_files(self) -> list[str]:
        workspace, repo, sha = self.get_identifiers()
        repo_dir = Path(self.env["BITBUCKET_CLONE_DIR"]).resolve(strict=True)
        try:
            files = (
                subprocess.check_output(["git", "diff", "--name-only", sha, 'HEAD'], cwd=repo_dir)
                .decode("utf8")
                .split("\n")
            )
            return [f for f in files if Path(repo_dir / f).is_file()]
        except:
            return []

    def prepare_archive(self):
        repo_dir = Path(self.env["BITBUCKET_CLONE_DIR"]).resolve(strict=True)

        archive_name = self.get_full_name() + ".zip"

        with ZipFile(archive_name, "w", compression=ZIP_DEFLATED) as zipf:
            for fp in repo_dir.rglob("*"):
                zipf.write(fp, fp.relative_to(repo_dir))

    def send_archive(self, submit_url):
        archive_name = self.get_full_name() + ".zip"
        headers = {"Content-Type": "application/zip"}
        with open(archive_name, "rb") as fp:
            content = fp.read()
            resp = requests.put(submit_url, data=content, headers=headers)

    def wait_for_result(self) -> list[dict]:
        workspace, repo, sha = self.get_identifiers()

        api_url = self.get_variable("API_URL")
        api_key = self.get_variable("API_KEY")
        headers = {
            "Authorization": f"Bearer {api_key}",
        }

        stop_time = time.time() + self.get_variable("POLLING_TIMEOUT")
        interval = self.get_variable("POLLING_INTERVAL_SECONDS")
        while stop_time > time.time():
            resp = requests.get(
                f"{api_url}/analysis/status",
                json={"workspace": workspace, "repository": repo, "commit_hash": sha},
                headers=headers,
            )
            if resp.status_code == 200:
                content = resp.json()
                if content["status"] == "failed":
                    self.fail(f"Failed with message: {content['output']['message']}")
                if content["status"] == "complete":
                    break
                logger.debug("Analysis running %s", content["output"]["message"])
            time.sleep(interval)
        try:
            return content["output"]["problems"]
        except KeyError:
            if self.get_variable("FAIL_ON_TIMEOUT"):
                self.fail("timed out waiting for analysis to complete")
            else:
                self.success(message="Check Metabob.com for anaylsis results")

    def print_results(self, problems: list[dict]):
        source_dir = Path(self.env["BITBUCKET_CLONE_DIR"]).resolve(strict=True)
        output_file = Path(self.get_variable("OUTPUT_FILE"))

        format_out = []
        problems_mapped: dict[str, list[dict]] = {}
        for problem in problems:
            if problem["path"] in problems_mapped:
                problems_mapped[problem["path"]].append(problem)
            else:
                problems_mapped[problem["path"]] = [problem]

        for path, path_problems in problems_mapped.items():
            with (source_dir / path).open("r") as fp:
                sourcelines = fp.readlines()
                for problem in path_problems:
                    data = {
                        "path": path,
                        "lineno": problem["startLine"],
                        "end_lineno": problem["endLine"],
                        "summary": problem["summary"],
                        "description": problem["description"],
                        "source": "\n".join(
                            sourcelines[problem["startLine"] : problem["endLine"]]
                        ),
                    }
                    format_out.append(data)

        output_file.parent.mkdir(parents=True, exist_ok=True)
        output_file.touch()

        with output_file.open("w") as fp:
            json.dump(format_out, fp)
        print(json.dumps(format_out, indent=1))

    def create_report(self, problems: list[dict]):
        workspace, repo, sha = self.get_identifiers()
        insights = CodeInsights(workspace, repo, sha)

        annotations = convert_metabob_problems_to_code_annotations(problems)

        pass_fail = "PASSED"
        if len(problems) > self.get_variable("FAIL_THRESHOLD"):
            pass_fail = "FAILED"

        report_id = str(uuid4())
        report_data = {
            "type": "report",
            "external_id": report_id,
            "title": "Metabob Report",
            "reporter": "Metabob",
            "report_type": "BUG",
            "logo_url": "https://metabob.com/images/metabob_nonwhiteBG.svg",
            "details": "Metabob Analysis Results",
            "result": pass_fail,
            "data": [
                {
                    "title": "Potential Problems Identified",
                    "type": "NUMBER",
                    "value": len(problems),
                }
            ],
        }

        insights.create_report(sha, report_data)

        for offset in range(
            0, len(annotations), 100
        ):  # Should never be close to 100 ideally
            for annotation in annotations[offset : (offset + 99)]:
                insights.create_annotation(sha, report_id, annotation)


if __name__ == "__main__":
    '''This is the entrypoint for the pipe'''
    pipe = MetabobPipe(pipe_metadata="/opt/app/pipe.yml", schema=schema)
    pipe.run()
