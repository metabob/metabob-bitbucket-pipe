import os
import subprocess

docker_image = "metabobapp/metabob-bitbucket-pipe:ci" + os.getenv(
    "BITBUCKET_BUILD_NUMBER", "local"
)


def docker_build():
    """
    Build the docker image for tests.
    :return:
    """
    args = [
        "docker",
        "build",
        "-t",
        docker_image,
        ".",
    ]
    subprocess.run(args, check=True)


def setup():
    docker_build()


def test_no_parameters():
    args = [
        "docker",
        "run",
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 1
    assert "✖ Validation errors: \nAPI_KEY:\n- required field" in result.stdout


def test_success():
    root_path = subprocess.run(
        ["git", "rev-parse", "--show-toplevel"],
        check=False,
        text=True,
        capture_output=True,
    ).stdout.strip()
    head_commit = subprocess.run(
        ["git", "rev-parse", "HEAD~1"],
        check=False,
        text=True,
        capture_output=True,
    ).stdout.strip()
    args = [
        "docker",
        "run",
        "-v",
        '--rm',
        f"{root_path}:/var/repo",
        "-e",
        "API_KEY=ab39ds9cnsdi",
        "-e",
        "BITBUCKET_CLONE_DIR=/var/repo",
        "-e",
        "BITBUCKET_WORKSPACE=workspace",
        "-e",
        "BITBUCKET_REPO_SLUG=repo_name",
        "-e",
        f"BITBUCKET_COMMIT={head_commit}",
        "-e",
        "FAIL_THRESHOLD=3",
        "-e",
        "POLLING_TIMEOUT=300",
        "-e",
        "POLLING_INTERVAL_SECONDS=10",
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert "" in result.stdout
