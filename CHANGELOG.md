# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.4.0

- minor: code insights new features

## 0.3.0

- minor: Added artifact output and made codeInsights optional
- patch: Added option to not fail on timeout
- patch: weekly-update

## 0.2.1

- patch: Better response on timeout

## 0.2.0

- minor: Beta release update

## 0.1.9

- patch: Remove CodeInsights Dependency and bactched annotation writes

## 0.1.8

- patch: Added logo and vendor information

## 0.1.7

- patch: Even more adjustments to annotations

## 0.1.6

- patch: More adjustments to annotation output

## 0.1.5

- patch: Rephrased insight report content

## 0.1.4

- patch: Handled case where analysis was already completed for a commit hash

## 0.1.3

- patch: Added polling interval option

## 0.1.2

- patch: Changed max waiting period to 10m

## 0.1.1

- patch: Updated for new API specs

## 0.1.0

- minor: Initial release

